import { Injectable, ComponentFactoryResolver } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public oktaAuth: OktaAuthService) { }

   isAdmin(){
    return this.oktaAuth.getAccessToken().then(token => {
      if(token){
        console.log(JSON.parse(atob(token.split('.')[1])))
      const group:string[] = JSON.parse(atob(token.split('.')[1]))['group'];
      if(group != null){
        for(var i =0; i<group.length;++i){
          console.log(group[i])
          if(group[i] === 'Admin'){
            return true;
          }
        }
        return false;
      }
      return false;
      }
      return false;
    })
  }

  getLastLoggedInTime(){
    return this.oktaAuth.getAccessToken().then(token => {
      const group= JSON.parse(atob(token.split('.')[1]))['lastLoginTime'];
      return group;
    })
  }
}

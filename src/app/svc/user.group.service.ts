import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserGroupService {

  constructor(private http: HttpClient) { }

  baseUrl = 'http://localhost:8080/';

  getAllUsers(){
    return this.http.get(this.baseUrl+'users');
  }

  makeMeAdmin(userId:string){
    const params = new HttpParams();
    return this.http.post(this.baseUrl+'makemeadmin', params.set('userId', userId));
  }

  create(body){
    return this.http.post(this.baseUrl+'create', body);
  }
}

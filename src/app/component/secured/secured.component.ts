import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/svc/auth.service';

@Component({
  selector: 'app-secured',
  templateUrl: './secured.component.html',
  styleUrls: ['./secured.component.css']
})
export class SecuredComponent implements OnInit {

  isAdmin = false;
  lastLogin: Date
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.auth.isAdmin().then(m => {
      this.isAdmin = m;
    })

    this.auth.getLastLoggedInTime().then(m => {
      this.lastLogin = m;
    })
  }

}

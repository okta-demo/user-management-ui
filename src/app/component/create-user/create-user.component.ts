import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserGroupService } from 'src/app/svc/user.group.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  userForm: FormGroup;
  constructor(private fb: FormBuilder, private userGroup: UserGroupService) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      phone: [null, Validators.required]
    })
  }

  create(){
    console.log(this.userForm)
    const body = {
      firstName: this.userForm.get('firstName').value,
      lastName: this.userForm.get('lastName').value,
      email: this.userForm.get('email').value,
    }

    this.userGroup.create(body).subscribe(x => {
      console.log(x)
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { UserGroupService } from 'src/app/svc/user.group.service';

@Component({
  selector: 'app-secured-admin',
  templateUrl: './secured-admin.component.html',
  styleUrls: ['./secured-admin.component.css']
})
export class SecuredAdminComponent implements OnInit {
  dataSource: any[];
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'admin'];
  constructor(private userGroup: UserGroupService) { }

  ngOnInit() {
    this.userGroup.getAllUsers().subscribe((x: any) => {
      console.log(x);
      this.dataSource = x.nonAdmin
    })
  }

  adminise(i){
    console.log(i)
    this.userGroup.makeMeAdmin(this.dataSource[i].id).subscribe(x => {
      console.log(x);
    })
  }

}

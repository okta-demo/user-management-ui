import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecuredAdminComponent } from './secured-admin.component';

describe('SecuredAdminComponent', () => {
  let component: SecuredAdminComponent;
  let fixture: ComponentFixture<SecuredAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecuredAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecuredAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

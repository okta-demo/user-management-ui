import { Component } from '@angular/core';
import { OktaAuthService } from '@okta/okta-angular';
import { AuthService } from './svc/auth.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'user-mgmt';

  isAuthenticated: boolean;
  isAdmin: boolean

  constructor(public oktaAuth: OktaAuthService, public authService: AuthService) {
    // Subscribe to authentication state changes
    this.oktaAuth.$authenticationState.subscribe(
      (isAuthenticated: boolean)  => this.isAuthenticated = isAuthenticated
    );
  }

  async ngOnInit() {
    // Get the authentication state for immediate use
    this.isAuthenticated = await this.oktaAuth.isAuthenticated();
    this.isAdmin = await this.authService.isAdmin()
  }

  login() {
    this.oktaAuth.loginRedirect('/secured');
  }

  logout() {
    this.oktaAuth.logout('/');
  }

}

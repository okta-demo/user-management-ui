import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OktaCallbackComponent } from '@okta/okta-angular';
import { SecuredComponent } from './component/secured/secured.component';
import { SecuredAdminComponent } from './component/secured-admin/secured-admin.component';
import { CreateUserComponent } from './component/create-user/create-user.component';

const routes: Routes = [
  {
    path: 'implicit/callback',
    component: OktaCallbackComponent
  },
  {
    path: 'secured',
    component: SecuredComponent
  },
  {
    path: 'secured/admin',
    component: SecuredAdminComponent
  },
  {
    path: 'secured/create',
    component: CreateUserComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
